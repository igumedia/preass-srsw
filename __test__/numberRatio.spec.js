const numberRatio = require("../numberRatio");

test("Number ratio of [-4,3,-9,0,4,1,5] is [0.571, 0.286, 0.143, 0.429, 0.571]", () => {
  expect(numberRatio([-4, 3, -9, 0, 4, 1, 5])).toEqual([
    0.571,
    0.286,
    0.143,
    0.429,
    0.571,
  ]);
});
