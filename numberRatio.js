/**
 * Calculate positive, negative, zero, even and odd number ratio in an array
 * @param {number[]} arr - Array of numbers to be calculated
 * @returns {number[]} Ratio of the positive, negative, zero, even and odd number
 */
function numberRatio(arr) {
  // enum
  let numberOfPositiveNumbers = 0;
  let numberOfNegativeNumbers = 0;
  let numberOfZerosNumbers = 0;
  let numberOfEvenNumbers = 0;
  let numberOfOddNumbers = 0;

  // iterate through each elements on the array
  arr.forEach((number) => {
    // check whether number is a positive number
    if (number > 0) numberOfPositiveNumbers++;

    // check whether number is a negative number
    if (number < 0) numberOfNegativeNumbers++;

    // check whether number is zero (0)
    if (number === 0) numberOfZerosNumbers++;

    // check whether even or odd number
    if (number % 2 === 0) {
      // number is an even number
      numberOfEvenNumbers++;
    } else {
      // number is an odd number
      numberOfOddNumbers++;
    }
  });

  /**
   * Internal function to divide the supplied number by the length of the array
   * @param {number} totalNumber - Number to be divided by the array length
   * @returns {number} Ratio of the supplied number
   */
  function calculateRatio(totalNumber) {
    // calculate number ratio and rounded to 3 decimal places
    let numberRatio = (totalNumber / arr.length).toFixed(3);

    // convert to number data type and return the value
    return parseFloat(numberRatio);
  }

  // return result to function caller
  return [
    calculateRatio(numberOfPositiveNumbers),
    calculateRatio(numberOfNegativeNumbers),
    calculateRatio(numberOfZerosNumbers),
    calculateRatio(numberOfEvenNumbers),
    calculateRatio(numberOfOddNumbers),
  ];
}

module.exports = numberRatio;
